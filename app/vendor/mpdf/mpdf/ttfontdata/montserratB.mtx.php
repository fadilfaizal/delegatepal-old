<?php
$name='Montserrat-SemiBold';
$type='TTF';
$desc=array (
  'CapHeight' => 700,
  'XHeight' => 538,
  'FontBBox' => '[-276 -284 1424 974]',
  'Flags' => 262148,
  'Ascent' => 968,
  'Descent' => -251,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 135,
  'MissingWidth' => 500,
);
$unitsPerEm=1000;
$up=-125;
$ut=50;
$strp=322;
$strs=50;
$ttffile='C:/xampp/htdocs/DelegatePal/app/vendor/mpdf/mpdf/ttfonts/Montserrat-Bold.ttf';
$TTCfontID='0';
$originalsize=67104;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='montserratB';
$panose=' 0 0 0 0 7 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 968, -251, 0
// usWinAscent/usWinDescent = 968, -251
// hhea Ascent/Descent/LineGap = 968, -251, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>